---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data below is as of 2018-08-31.

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 343   | 100%        |
| Based in the US                           | 196   | 57.14%      |
| Based in the UK                           | 21    | 6.12%       |
| Based in the Netherlands                  | 14    | 4.08%       |
| Based in Other Countries                  | 112   | 32.65%      |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 343   | 100%        |
| Men                                       | 271   | 86.03%      |
| Women                                     | 71    | 22.54%      |
| Other Gender Identities                   | 1     | 0%          |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 31    | 100%        |
| Men in Leadership                         | 25    | 80.65%      |
| Women in Leadership                       | 6     | 19.35%      |
| Other Gender Identities                   | 0     | 0%          |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 141   | 100%        |
| Men in Development                        | 127   | 89.31%      |
| Women in Development                      | 14    | 9.92%      |
| Other Gender Identities                   | 0     | 0%          |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 196   | 100%        |
| Asian                                     | 15    | 7.65%       |
| Black or African American                 | 3     | 1.53%       |
| Hispanic or Latino                        | 11    | 5.61%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.51%       |
| Two or More Races                         | 9     | 4.59%       |
| White                                     | 103   | 52.55%      |
| Unreported                                | 54    | 27.55%      |

| Race/Ethnicity in Development   (US Only) | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 46    | 100%        |
| Asian                                     | 4     | 8.70%       |
| Black or African American                 | 1     | 2.17%       |
| Hispanic or Latino                        | 3     | 6.52%       |
  Two or More Races                         | 3     | 6.52%       |
| White                                     | 22    | 47.83%      |
| Unreported                                | 13    | 28.26%      |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 25    | 100%        |
| Asian                                     | 3     | 12.00%      |
| Native Hawaiian or Other Pacific Islander | 1     | 4.00%       |
| White                                     | 11    | 44.00%      |
| Unreported                                | 10    | 40.00%      |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 343   | 100%        |
| Asian                                     | 29    | 8.45%       |
| Black or African American                 | 6     | 1.75%       |
| Hispanic or Latino                        | 18    | 5.25%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.29%       |
| Two or More Races                         | 10    | 2.92%       |
| White                                     | 177   | 51.60%      |
| Unreported                                | 102   | 29.74%      |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 141   | 100%        |
| Asian                                     | 11    | 7.80%       |
| Black or African American                 | 3     | 2.13%       |
| Hispanic or Latino                        | 9     | 6.38%       |
| Two or More Races                         | 4     | 2.84%       |
| White                                     | 71    | 50.35%      |
| Unreported                                | 43    | 30.50%      |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 31    | 100%        |
| Asian                                     | 3     | 9.68%       |
| Hispanic or Latino                        | 1     | 3.23%       |
  Native Hawaiian or Other Pacific Islander | 1     | 3.23%       |
| White                                     | 12    | 38.71%      |
| Unreported                                | 14    | 45.16%      |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 343   | 100%        |
| 18-24                                     | 18    | 5.59%       |
| 25-29                                     | 78    | 22.74%      |
| 30-34                                     | 94    | 27.41%      |
| 35-39                                     | 59    | 17.20%      |
| 40-49                                     | 60    | 17.49%      |
| 50-59                                     | 31    | 9.32%       |
| 60+                                       | 2     | 0.58%       |
| Unreported                                | 1     | 0.29%       |
