---
layout: markdown_page
title: "Gartner Magic Quadrant for Application Release Orchestration 2018"
---

## GitLab and the Gartner Magic Quadrant for Application Release Orchestration 2018*
This page represents how Gartner views our application release orchestration capabilities in relation to the larger market and how we're working with that information to build a better product. It also provides Gartner with ongoing context for how our product is developing in relation to how they see the market.

![Gartner MQ ARO](images/home/gartner-mq-application-release-orchestration-graphic.jpg){: .small}

### Gartner's Key Takeaways on the ARO Market at time of report publication:

**Strategic Planning Assumption**
By 2023, 75% of global enterprises will have implemented at least one application release orchestration (ARO) solution, which is a substantial increase from fewer than 20% today.

**Market Definition/Description**
ARO tools provide a combination of deployment automation, pipeline and environment management, and release orchestration capabilities to simultaneously improve the quality, velocity and governance of application releases. ARO tools enable enterprises to scale release activities across multiple, diverse and multigenerational teams (e.g., DevOps), technologies, development methodologies (agile, etc.), delivery patterns (e.g., continuous), pipelines, processes and their supporting toolchains.

**Note:** Magic Quadrant for Application Release Orchestration, Colin Fletcher, Laurie Wurster, Christopher Little, 10 September 2018.
{: .note .font-small .margin-top40}

This graphic was published by Gartner, Inc. as part of a larger research document and should be evaluated in the context of the entire document. The Gartner document is available upon request from GitLab.
{: .note .font-small}

Gartner does not endorse any vendor, product or service depicted in its research publications, and does not advise technology users to select only those vendors with the highest ratings or other designation. Gartner research publications consist of the opinions of Gartner’s research organization and should not be construed as statements of fact. Gartner disclaims all warranties, expressed or implied, with respect to this research, including any warranties of merchantability or fitness for a particular purpose.
{: .note .font-small}
