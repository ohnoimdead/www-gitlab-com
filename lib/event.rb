require 'yaml'

module Gitlab
  module Homepage
    class Event
      include Comparable

      def initialize(data)
        @data = data
      end

      # Some dates are actually a range. Need to get the start date
      # for event sort comparison.
      def sort_date
        @sort_date ||= Date.parse(date.gsub(/(\s+)(\d+)\s*-\s*\d+,/, '\1\2,'))
      end

      def <=>(other)
        sort_date <=> other.sort_date
      end

      ##
      # Middeman Data File objects compatibiltiy
      #
      def method_missing(name, *args, &block) # rubocop:disable Style/MethodMissing
        @data[name.to_s]
      end

      def self.all!
        @events ||= YAML.load_file('data/events.yml')
        @events.map do |data|
          new(data)
        end
      end
    end
  end
end
