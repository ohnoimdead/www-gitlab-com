---
layout: job_page
title: "People Operations Analyst"
---

## Responsibilities

### Intermediate People Operations Analyst

- Coordinate on compensation strategies and principles.
- HRIS data entry.
- Process HRIS changes related to events, such as hiring, termination, leaves, transfers, or promotions.
- Audit all system changes to ensure accuracy.
- Coordinate with the People Ops team on People Operations policies, processes and procedures following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Update people operations documentation as directed.
- Coordinate on benefits administration.
- Collect data to track trends in functional areas.
- Assist with training employees on various topics.
- Process and document all visa related applications and questions for travel.
- Keep it efficient and DRY.

### Senior People Operations Analyst

- Develop compensation strategies and principles.
- HRIS data entry, ensuring Data Integrity and alignment with all ancillary systems.
- Process HRIS changes related to events, such as hiring, termination, leaves, transfers, or promotions.
- Audit all system changes to ensure accuracy.
- Implement People Operations policies, processes and procedures following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Maintain people operations documentation.
- Benefits management.
- Collect and analyze data to track trends in functional areas.
- Ensure compliance with all international rules and regulations.
- Manage special projects.
- Collaborate on training employees on various topics.
- Manage and document all visa related applications and questions for travel.
- Keep it efficient and DRY.

### Staff People Operations Analyst

- Manage and implement compensation strategies and principles.
- HRIS management, ensuring Data Integrity and alignment with all ancillary systems.
- Manage HRIS changes related to events, such as hiring, termination, leaves, transfers, or promotions.
- Audit all system changes to ensure accuracy.
- Develop and implement People Operations policies, processes and procedures following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Manage people operations documentation.
- Benefits management and development of benefit principles.
- Collect and analyze data to track trends in functional areas and implement changes based on the findings.
- Ensure compliance with all international rules and regulations.
- Manage special projects.
- Train employees on various topics.
- Manage, implement, and document all visa related applications and questions for travel. Act as a mentor for any employment related visas when needed.
- Keep it efficient and DRY.

## Requirements

- Prior extensive experience in an HR or People Operations role
- Clear understanding of HR laws in one or multiple countries where GitLab is active
- Ability to work strange hours when needed (for example, to call an embassy in a different continent)
- Excellent written and verbal communication skills
- Enthusiasm for, and broad experience with, software tools
- Proven experience quickly learning new software tools
- Willing to work with git and GitLab whenever possible
- Willing to make People Operations as open and transparent as possible
- Wanting to work for a fast moving startup
- You share our [values](/handbook/values), and work in accordance with those values
- The ability to work in a fast paced environment with strong attention to detail is essential.
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).
